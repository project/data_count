(function ($, Drupal) {
  Drupal.behaviors.data_count = {
    attach: function (context, settings) {
      $('span#node-count').click(function () {
        $('.node-details').show('slow');
        $('.user-details').hide();
        $('span#user-count').removeClass('btn-effect');
        $(this).addClass('btn-effect');
      });
      $('span#user-count').click(function () {
        $('.node-details').hide();
        $('.user-details').show('slow');
        $('span#node-count').removeClass('btn-effect');
        $(this).addClass('btn-effect');
      });
    }
  };
})(jQuery, Drupal);
