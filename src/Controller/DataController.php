<?php

namespace Drupal\data_count\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Return renderable array response for template.
 */
class DataController extends ControllerBase {

  /**
   * Render array for the content.
   */
  public function dataCount() {
    $items = [
      'nodes' => data_count_node(),
      'users' => data_count_user(),
    ];
    $data['#attached']['library'] = ['data_count/count-style'];
    $data['#theme'] = 'data_count';
    $data['#data'] = $items;
    return $data;
  }

}
