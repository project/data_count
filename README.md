# Introduction

A statistical module, it helps admin to count all existing nodes and users data
 on the website. Admin can easily monitor the status of published/unpublished
  nodes and active/inactive users through given links respectively.

Further, it provides the number of published/unpublished nodes in each
 content type and the number of valued active/inactive users based on
  assigned roles so such details can be useful in content audits.

For a full description of the module, visit the
[Data Count](https://www.drupal.org/project/data_count).


## List of contents

- Requirements
- Installation
- Steps to use
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install the module as usual.


## Steps to use

1. Enable the module via Drush or the admin interface.
2. Go to the admin page "Reports >> Data Count" to view results.
3. The page shows the content type wise count under "Node Count Details" tab.
4. Similarly, there is the user role wise count under "User Count Details" tab.


## Maintainers

- Hemant Gupta - [hemant-gupta](https://www.drupal.org/u/hemant-gupta)
